import {Selector} from 'testcafe'
import {filters, getBrand} from '../helpers/selectors/pp'
import {brand} from '../helpers/selectors/pp'
import {price} from '../helpers/selectors/pp'
import {makeupPage} from '../helpers/selectors/pp'
import {urls} from '../helpers/urls'

const {brandFilter,priceFilter} = filters
const {filterTags, productCount} = makeupPage
const {under49}= price


fixture `As a guest I want to select multiple filters and see the product list according to the filters`
 .page `${urls.second}`;


 test('search', async t =>{
    await t
    .maximizeWindow()
    .click(brandFilter)
    .click(getBrand('brand1'))
    .click(priceFilter)
    .click(under49)
    await t
    .expect(Selector(productCount).withText('11 items').exists).ok()
    .expect(Selector(filterTags).withText('Under 49.99').exists).ok()
    .expect(Selector(filterTags).withText('Dior Beaute').exists).ok()

}).meta('tag','@smoke')