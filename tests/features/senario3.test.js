import {Selector} from 'testcafe'
import {filters, brand,price, makeupPage} from '../helpers/selectors/pp'
import {eyeShadowPage} from '../helpers/selectors/ppl'
import {urls} from '../helpers/urls'

const {header, sort, lowest, productGridItem, priceWas}= eyeShadowPage

fixture `As a guest When I land on this url :`
 .page `${urls.third}`;


 test('validation', async t =>{
    await t
    .maximizeWindow()
 .click(sort)
 .click(lowest)
 
 const grids = await Selector(productGridItem)
 const getPriceOfGrid = async (number) => {
     const text = await grids.nth(number).find(priceWas).innerText
     console.log(text)
     return Number(text.replace('$',''))
 }
 const firstPrice = await getPriceOfGrid(0)
 const thirdPrice = await getPriceOfGrid(1)
 await t.expect(thirdPrice).gt(firstPrice)  
})