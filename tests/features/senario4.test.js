import { urls } from "../helpers/urls"
import { addToBag, resizeTheWindow } from "../helpers/selectors/pp"
import { Selector } from "testcafe"
import { scrollTo, reload } from "../helpers/browser"
const {stickyAddToBag, staticAddToBag} = addToBag

fixture `As a guest when I view this url : in mobile devices size : width : 411 , height:823`
.page `${urls.productPage}`
.beforeEach( async t => {
    await resizeTheWindow(t, 'mobile')
})

test('Given scroll:0, It should not show sticky or static button', async t =>{
    await reload(t)
    await scrollTo(0)
    await t.expect(Selector(stickyAddToBag).exists).notOk()
    await t.expect(Selector(staticAddToBag).exists).ok()
   
})

test('Given scroll:101, It should show sticky and not static button', async t =>{
    await reload(t)
    await scrollTo(101)
    await t.expect(Selector(stickyAddToBag).exists).ok()
    await t.expect(Selector(staticAddToBag).exists).notOk()
})
test('Given scroll:300, It should not show sticky and just show static button', async t =>{
    await reload(t)
    await scrollTo(300)
    await t.expect(Selector(stickyAddToBag).exists).notOk()
    await t.expect(Selector(staticAddToBag).exists).ok()
})
test('Given scroll:1214, It should show sticky and not static button', async t =>{
    await reload(t)
    await scrollTo(1214)
    await t.expect(Selector(stickyAddToBag).exists).ok()
    await t.expect(Selector(staticAddToBag).exists).notOk()
})
test('Given scroll:4655, It should not show sticky and just show static button', async t =>{
    await reload(t)
    await scrollTo(4655)
    console.log(await Selector(stickyAddToBag).exists)
    await t.expect(Selector(stickyAddToBag).exists).notOk()
    await t.expect(Selector(staticAddToBag).exists).ok()
})


