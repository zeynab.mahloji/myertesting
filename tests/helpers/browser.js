import { ClientFunction } from 'testcafe'
// const getScrollY = ClientFunction(() => window.scrollY)

export const scrollTo = ClientFunction((y) => window.scrollTo(0,y))

export const reload = async(t) => {
    await t.eval(() => location.reload(true))
}