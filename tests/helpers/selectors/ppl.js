
export const eyeShadowPage = {
    'header' : '[data-automation="product-listing-title"]',
    'sort' : '#product-sort-dropdown',
    'lowest' : '[data-automation="sort-by-priceAsc"]',
    'productGridItem': '[data-automation="product-grid-item"]',
    'priceWas' : '[data-automation="product-price-was"]'
   
    
}