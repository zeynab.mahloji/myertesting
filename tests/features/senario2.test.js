import {Selector} from 'testcafe'
import {filters, brand,price, makeupPage} from '../helpers/selectors/pp'
import {eyeShadowPage} from '../helpers/selectors/ppl'
import {urls} from '../helpers/urls'
const {brandFilter,priceFilter} = filters
const {filterTags, productCount} = makeupPage
const {diorBeaute}= brand
const {under49}= price
const {header}= eyeShadowPage

fixture `As a guest When I land on this url :`
 .page `${urls.third}`;


 test('validation', async t =>{
    await t
    .maximizeWindow()

    .expect(Selector(productCount).withText('3 items').exists).ok()
    .expect(Selector(header).withText('Eyeshadow').exists).ok()
    .expect(Selector(filterTags).withText('49.99 to 100').exists).ok()
    .expect(Selector(filterTags).withText('Clinique').exists).ok()
 
})