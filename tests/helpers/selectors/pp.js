

export const filters ={
    'brandFilter' : '[data-automation="brand_desktop_filter"]',
    'priceFilter' : '[data-automation="price_desktop_filter"]'
    
}

const brand = {
        'brand1'   :  '[data-automation="Dior Beaute"]', 
}
export const getBrand = (key) => {
    return brand[stack][key]
}
export const price = {
    'under49' : '[data-automation="Under 49.99"]'  
}
export const screenSize = {
    'mobile' : {
        x: 411,
        y: 823
    },
    'tablet':{
        x: 800,
        y: 1024
    }
} 

export const resizeTheWindow = async (t, device) => {
    const {x, y} = screenSize[device]
    await t.resizeWindow(x, y)
}

export const addToBag ={
    'stickyAddToBag' : 'button[automation="sticky"]',
    'staticAddToBag' : 'button[automation="static"]'
} 

export const makeupPage = {
    'filterTags' : '[data-automation="applied-filter-tags"]',
    'productCount' : '[data-automation="product-count"]'
    
    
}